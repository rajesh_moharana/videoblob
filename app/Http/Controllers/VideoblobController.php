<?php

namespace App\Http\Controllers;

use App\videoblob;
use FFMpeg;
use Illuminate\Http\Request;

class VideoblobController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_video');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' =>'required',
            'publish_date' =>'required',
            'video'  => 'required|mimes:mp4',
            'poster' =>'required|image|mimes:jpg,png,gif',
           ]);
            $poster = $request->file('poster');
            $new_poster = rand() . '.' . $poster->getClientOriginalExtension();
            $poster->move(public_path('images'), $new_poster);
            $video = $request->file('video');
            $new_video = rand() . '.' . $video->getClientOriginalExtension();
            $video->move(public_path('videos/original'), $new_video);
            copy(public_path('videos/original').'/'.$new_video,public_path('videos/preview').'/'.$new_video);
            $videoblob = new videoblob;

            $videoblob->title = $request->title;
            $videoblob->publish_date = date('Y-m-d',strtotime($request->publish_date));
            $videoblob->is_active = 1;
            $videoblob->video = $new_video;
            $videoblob->poster = $new_poster;
            $videoblob->save();
            try{
                $ffmpeg = FFMpeg\FFMpeg::create();
                $short_video=public_path('videos/preview').'/'.$new_video;
                $attributes=_get_video_attributes($short_video,$ffmpeg);
                $preview_time=$attributes['secs']*02;
                $video = $ffmpeg->open($short_video);
                $clip = $video->clip(FFMpeg\Coordinate\TimeCode::fromSeconds($preview_time), FFMpeg\Coordinate\TimeCode::fromSeconds($preview_time));
            $clip->save(new FFMpeg\Format\Video\X264(), $short_video);
            }catch(Exception $e){
                return redirect()->route('videoblob.create');
            }
            finally {
                return redirect()->route('videoblob.create');
            }

        // echo 3;exit;
       return redirect()->route('videoblob.create');
    }

    public function fetchvideo(){
        $videoblob = Videoblob::where('publish_date', '<=', date('Y-m-d'))->select('id','title','video','poster')->get();
        return response()->json(['data' => $videoblob->toArray()], 200);
    }

}
